Test app for Crystal
-----------------
First install `docker compose`, then clone repo.
Enter app directory and run `docker-compose up --build` to start web app with admin panel (if you need)
http://127.0.0.1:8000/admin/

login: admin

pass: admin


####To stars script run:
Insert random data to raw_data table:
`docker-compose run server python manage.py init_db_data`

source - https://bitbucket.org/antero151/crystal-test/src/master/core/management/commands/init_db_data.py

Aggregate data and write to agg_data table
`docker-compose run server python manage.py agg_data`

source - https://bitbucket.org/antero151/crystal-test/src/master/core/management/commands/agg_data.py
  
--------------

Представим, что есть база юзеров и ивентов в которых они участвуют. В каждом ивенте юзер либо зарабатывает деньги, либо тратит их. Задача состоит в подсчёте статистики для юзера. 

СУБД можно использовать любую, на свой вкус. Предлагаемая схема данных ниже, но её можно изменять, главное чтобы все нужные данные сохранялись. 

В таблице raw_data складываются подряд данные по каждому участию юзера в ивенте. Amount может быть положительным (юзер заработал) или отрицательным (потратил)
raw_data (
   id int auto increment primary key,
   user_id unsigned int,
   event_id unsigned int,
   amount int
)

В таблице agg_data собираются статистические данные по каждому юзеру: balance - сумма amount по всем ивентам, event_number - количество ивентов, в которых юзер участвовал, best_event_id - ID ивента в котором у юзера максимальный amount, worst_event_id - ID ивента в котором у юзера минимальный amount.
agg_data (
   user_id unsigned int primary key,
   balance int,
   event_number unsigned int,
   best_event_id unsigned int,
   worst_event_id unsigned int
)

Таблица last_processed_id - служебная, подробности дальше.
last_processed_id (
   id number
)

Необходимо написать два скрипта
1) Заполнение базы сырыми данными (таблица raw_data). Сырые данные генерируются случайным образом. Если таблица уже заполнена данными, то скрипт просто выходит. Сгенерировать надо случайное число строк между 90000 и 100000. При этом параметры user_id и event_id могут принимать значения от 1 до 1000, а amount от -100000 до 100000. Пара (user_id,event_id) должна быть уникальная.
2) Скрипт-аггрегатор. Скрипт за один запуск должен вытаскивать не более 10000 строк из базы в сыром виде, обсчитывать необходимые значения в памяти (т.е. без агрегирующих функций на стороне базы) и обновлять данные в agg_data по каждому из юзеров. После прохода он должен писать последний обработанный id в last_processed_id, и при следующем запуске начинать с этого id. Необходимо запустить скрипт столько раз, сколько потребуется для прохода по всем данным raw_data
