from django.contrib import admin
from .models import *


class AggDataAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'event_number',)


admin.site.register(RawData)
admin.site.register(AggData, AggDataAdmin)
admin.site.register(LastProcessId)
