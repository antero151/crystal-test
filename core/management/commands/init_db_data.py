from datetime import datetime
from django.db import connection

from django.core.management.base import BaseCommand, CommandError
import random
import itertools
from core.models import RawData


class Command(BaseCommand):
    help = 'generate random data to db'
    user_count = random.randint(1, 1001)
    event_count = random.randint(1, 1001)
    row_count = random.randint(90000, 100001)

    @classmethod
    def generate_new_data(cls):
        cls.user_count = random.randint(1, 1001)
        cls.event_count = random.randint(1, 1001)

    @classmethod
    def insert_row_gen(cls, pairs):
        for i, val in enumerate(pairs):
            if i < cls.row_count:
                yield val
            else:
                break

    @classmethod
    def check_random_pairs_count(cls):
        users_and_events = [i for i in range(1, cls.user_count+1)] + [i for i in range(1, cls.event_count+1)]
        unique_pairs = cls.get_random_pairs(users_and_events)
        if len(unique_pairs) < cls.row_count:
            cls.generate_new_data()
            cls.check_random_pairs_count()
        else:
            return cls.insert_row_gen(unique_pairs)

    @staticmethod
    def get_random_pairs(numb_list):
        pairs = set(list(itertools.permutations(numb_list, 2)))
        return pairs

    def handle(self, *args, **options):
        if RawData.objects.all().count() == 0:
            start = datetime.now()
            data = []
            for pairs in self.check_random_pairs_count():
                raw_obj = RawData(
                    user_id=pairs[0],
                    event_id=pairs[1],
                    amount=random.randint(-100000, 100000)
                )
                data.append(raw_obj)
            RawData.objects.bulk_create(data)
            end = datetime.now()
            print('done')
            print(end-start)
        else:
            print('Table not empty')
