import copy
from datetime import datetime

from django.core.management import BaseCommand

from core.models import AggData, RawData, LastProcessId


class Command(BaseCommand):

    @staticmethod
    def agg_gen(data):
        for obj in data:
            yield obj

    @staticmethod
    def clean_obj(obj):
        copy_obj = copy.deepcopy(obj[1])
        copy_obj.pop('best_event_amount', None)
        copy_obj.pop('worst_event_amount', None)
        copy_obj.pop('amount', None)
        copy_obj['user_id'] = obj[0]
        return copy_obj

    def handle(self, *args, **options):
        start = datetime.now()

        last_proc_id = LastProcessId.objects.last()

        if last_proc_id:
            data = RawData.objects.filter(pk__gt=last_proc_id.proc_id).values('id', 'user_id', 'event_id', 'amount')[:10000]
        else:
            data = RawData.objects.values('id', 'user_id', 'event_id', 'amount')[:10000]
        if data:
            result = {}
            for i in self.agg_gen(data):
                try:
                    obj_data = result[i.get('user_id')]
                    obj_data['balance'] += i.get('amount')
                    obj_data['event_number'] += 1
                    if i.get('amount') > obj_data['best_event_amount']:
                        obj_data['best_event_id'] = i.get('event_id')
                        obj_data['best_event_amount'] = i.get('amount')
                    elif i.get('amount') < obj_data['worst_event_amount']:
                        obj_data['worst_event_id'] = i.get('event_id')
                        obj_data['worst_event_amount'] = i.get('amount')
                except KeyError:
                    result[i.get('user_id')] = {
                        'balance': i.get('amount'),
                        'event_number': 1,
                        'best_event_id': i.get('event_id'),
                        'best_event_amount': i.get('amount'),
                        'worst_event_id': i.get('event_id'),
                        'worst_event_amount': i.get('amount'),
                        'amount': i.get('amount'),
                    }
            for i in self.agg_gen(result.items()):
                agg_obj = self.clean_obj(i)
                try:
                    agg = AggData.objects.get(user_id=agg_obj.get('user_id'))
                    agg.balance += agg_obj.get('balance')
                    agg.event_number += agg_obj.get('event_number')
                    agg.best_event_id = agg_obj.get('best_event_id')
                    agg.worst_event_id = agg_obj.get('worst_event_id')
                    agg.save()
                except AggData.DoesNotExist:
                    AggData.objects.create(**agg_obj)
            LastProcessId.objects.create(proc_id=data[len(data)-1].get('id'))

        end = datetime.now()

        print("done")
        print(end-start)
