from django.db import models


class RawData(models.Model):
    user_id = models.IntegerField()
    event_id = models.IntegerField()
    amount = models.IntegerField()

    class Meta:
        unique_together = ('user_id', 'event_id')


class AggData(models.Model):
    user_id = models.IntegerField(unique=True)
    balance = models.IntegerField()
    event_number = models.IntegerField()
    best_event_id = models.IntegerField()
    worst_event_id = models.IntegerField()

    def __str__(self):
        return str(self.user_id)


class LastProcessId(models.Model):
    proc_id = models.IntegerField()

    def __str__(self):
        return str(self.proc_id)
